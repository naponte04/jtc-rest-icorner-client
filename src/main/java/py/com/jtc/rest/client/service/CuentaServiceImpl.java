package py.com.jtc.rest.client.service;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import py.com.jtc.rest.client.bean.Cuentas;

@Service
public class CuentaServiceImpl  implements CuentaService{
	private static List<Cuentas> cuentas;
	
	@Override
	public List<Cuentas> obtenerCuenta(String nrodocumento) {
		RestTemplate restTemplate = new RestTemplate();
		ResponseEntity<Cuentas[]> responseEntity = restTemplate.getForEntity("http://localhost:8090/rest-client/cuentas/"+nrodocumento, Cuentas[].class);
		
		cuentas = Arrays.asList(responseEntity.getBody());
		return cuentas;
	}
	
	
	
	/*@Override
	public List<Cuentas>  obtenerCuenta(String nroDocumento) {
		//
		RestTemplate restTemplate = new RestTemplate();
		List<Cuentas> cuenta = restTemplate.getForObject("http://localhost:8090/cuentas/{nroDocumento}", Cuentas[].class,nroDocumento);
		return cuenta;
	}*/

	/*@Override
	public void insertarEmpleado(Cuentas cuenta) {
		//pendiente
		RestTemplate restTemplate = new RestTemplate();
		restTemplate.postForObject("http://localhost:8090/cuentas", cuenta, Cuentas.class);		
		
	}*/
	
	/*@Override
	public Cuentas editarEmpleado(Integer idcuenta, Cuentas cuenta) {
		// Actualizar lo necesario en esta funcion
		Map<String, Integer> params = new HashMap<String, Integer>();
		params.put("idCuenta", idcuenta);
		RestTemplate restTemplate = new RestTemplate();
		restTemplate.put("http://localhost:8090/cuentas/{idCuenta}", cuenta, params);
		Cuentas e = obtenerEmpleado(cuenta.getIdCuenta());
		return e;
	}*/
	
	/*@Override
	public void eliminarEmpleado(Integer idcuenta) {
		Map<String, Integer> params = new HashMap<String, Integer>();
	    params.put("idCuenta",idcuenta);
	    RestTemplate restTemplate = new RestTemplate();
	    restTemplate.delete("http://localhost:8090/cuentas/{idCuenta}",params);
	}*/

}
