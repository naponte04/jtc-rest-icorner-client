package py.com.jtc.rest.client.reports;

import java.io.InputStream;
import java.util.HashMap;
import java.util.List;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.xml.JRXmlLoader;

public class ReportUtil 

{
	
	public static JasperPrint generarReporte(String ListaEmpleados, List datosReporte, HashMap parametrosReporte) throws JRException{

	
		//Definimos cual sera nuestra fuente de datos
	JRBeanCollectionDataSource ds = new JRBeanCollectionDataSource(datosReporte);
	
	//Recuperamos el archivo fuente del reporte
	InputStream is = ClassLoader.getSystemClassLoader().getResourceAsStream(ListaEmpleados);
	JasperDesign jd = JRXmlLoader.load(is);
	
	//Compilamos el informe jrxml
	JasperReport report = JasperCompileManager.compileReport(jd);
	//Rellenamos el informe con el datasource y sus parametros
	JasperPrint print = JasperFillManager.fillReport(report, parametrosReporte, ds);
	//Retornamos el JasperPrint, para mandarlo al cliente via HTTP, o para generar en un pdf en disco
	return print;
	}
}
