package py.com.jtc.rest.client.configuration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.stereotype.Component;

import py.com.jtc.rest.client.service.LoginService;

@Component
public class AuthenticationService implements AuthenticationProvider {
  
	@Autowired
	private LoginService loginService;
	
	@Override
	public Authentication authenticate(Authentication auth) throws AuthenticationException {
		String nrodocumento = auth.getName();
		String pin = (String) auth.getCredentials();
		
		
		boolean logueado = loginService.login(nrodocumento, pin);
		
		if (logueado) {
			
			return new UsernamePasswordAuthenticationToken(nrodocumento, pin, null);
		}
		
		
		return null;
	}

	@Override
	public boolean supports(Class<?> auth) {
		// TODO Auto-generated method stub
		return auth.equals(UsernamePasswordAuthenticationToken.class);
	}

}
