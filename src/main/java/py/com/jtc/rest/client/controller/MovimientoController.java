package py.com.jtc.rest.client.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import py.com.jtc.rest.client.service.LoginService;
import py.com.jtc.rest.client.service.MovimientoService;


@Controller
@RequestMapping("/rest-client")
public class MovimientoController {
	@Autowired
	private MovimientoService movimientoService;
	@Autowired
	private LoginService loginService;
		
	@GetMapping("/movimientos")
	public String obtenerMovimiento(Model model) {
     Authentication auth = SecurityContextHolder.getContext().getAuthentication();
	 
		if (auth != null) {
		
		model.addAttribute("movimientos", movimientoService.obtenerMovimiento(auth.getName()));
		return "movimiento";
		}
		return null;
		
	}
	

}
