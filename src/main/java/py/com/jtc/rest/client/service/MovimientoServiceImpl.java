package py.com.jtc.rest.client.service;

import java.util.Arrays;
import java.util.List;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import py.com.jtc.rest.client.bean.Movimientos;


@Service
public class MovimientoServiceImpl implements MovimientoService{
	private static List<Movimientos> movimientos;
	
	@Override
	public List<Movimientos> obtenerMovimiento(String nrodocumento) {
		RestTemplate restTemplate = new RestTemplate();
		ResponseEntity<Movimientos[]> responseEntity = restTemplate.getForEntity("http://localhost:8090/rest-client/movimientos/"+nrodocumento, Movimientos[].class);
		
		movimientos = Arrays.asList(responseEntity.getBody());
		return movimientos;
	}
}	

