package py.com.jtc.rest.client.service;

import java.util.List;

import py.com.jtc.rest.client.bean.Cuentas;

public interface CuentaService {
	
	//<Cuentas> obtenerEmpleados();
	
	List<Cuentas> obtenerCuenta(String nrodocumento);
	
	/*void insertarEmpleado(Cuentas comentario);

	Cuentas editarEmpleado(Integer idcuenta, Cuentas e);
	
	void eliminarEmpleado(Integer idcuenta);*/
}
