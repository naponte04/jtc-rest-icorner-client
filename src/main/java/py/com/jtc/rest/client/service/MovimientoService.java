package py.com.jtc.rest.client.service;

import java.util.List;

import py.com.jtc.rest.client.bean.Movimientos;

public interface MovimientoService {
	
	List<Movimientos> obtenerMovimiento(String nrodocumento);

}
