package py.com.jtc.rest.client.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import py.com.jtc.rest.client.bean.User;

@Service
public class LoginServiceImpl implements LoginService {

	private static final String REST_CONTACTO_URL = "http://localhost:8090/login/";
	
	@Autowired
	private RestTemplate restTemplate;
	
	@Override
	public boolean login(String nrodocumento, String pin) {
		
		User user = new User();
		
		user.setNroDocumento(nrodocumento);
		user.setPin(pin);
		
		
		UriComponentsBuilder urlBuilder =  UriComponentsBuilder.fromHttpUrl(REST_CONTACTO_URL);
		urlBuilder.path("nrodocumento");
		
		Boolean response = restTemplate.postForObject(urlBuilder.toUriString(), user, Boolean.class);
		
		return response;
	}

}