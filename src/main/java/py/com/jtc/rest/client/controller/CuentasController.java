package py.com.jtc.rest.client.controller;



import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;


import py.com.jtc.rest.client.bean.Cuentas;
import py.com.jtc.rest.client.service.CuentaService;
import py.com.jtc.rest.client.service.LoginService;
import py.com.jtc.rest.client.service.LoginServiceImpl;



@Controller
@RequestMapping("/rest-client")
public class CuentasController {
	
	@Autowired
	private CuentaService cuentaService;

	@Autowired
	private LoginService loginService;

	
	@GetMapping
	public String index() {
		return "index";
	}
	
		
	@GetMapping("/cuentas")
	public String obtenerCuenta(Model model) {
     Authentication auth = SecurityContextHolder.getContext().getAuthentication();
	 
		if (auth != null) {
		
		model.addAttribute("cuentas", cuentaService.obtenerCuenta(auth.getName()));
		return "lista";
		}
		return null;
		
	}
	
	@GetMapping("/cuentas/add")
	public String empleadoForm(Model model) {
		model.addAttribute("cuenta", new Cuentas());
		return "form";
	}
	
	/*@GetMapping("/empleados/edit/{id}")
	public String empleadoEdit(@PathVariable("id") Integer id, Model model){
		// Pendiente.. cargar datos del empleado a editar
		model.addAttribute("empleado", empleadoService.obtenerEmpleado(id));
	return "form";
  }*/
		
	
	/*@PostMapping("/cuentas")
	public String agregarEmpleado(@ModelAttribute("cuenta") Cuentas emp) {
		// Pendiente.. persistir datos
		if(emp.getIdCuenta()!= null){
			empleadoService.editarEmpleado(emp.getIdCuenta(), emp);
		} else {
			empleadoService.insertarEmpleado(emp);
		}
		return "redirect:/rest-client/empleados";
	}*/
	
	/*@GetMapping("/empleados/delete/{id}")
	public String deleteEmpleado(@PathVariable("id") int id) {
		empleadoService.eliminarEmpleado(id);
		return "redirect:/rest-client/empleados";
	}*/

	
	
}
